package com.example.restapi.customer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.restapi.customer.customer;

@RestController
@RequestMapping("/api/v1/customers")
@Slf4j
@RequiredArgsConstructor

public class customerAPI {
    @Autowired
    private customerService vCustomerService;

    // public customerAPI(customerService _vCustomerService) {
    //     this.vCustomerService = _vCustomerService;
    // }

    @GetMapping("/hello")
    public ResponseEntity GetDef()
    {
        return ResponseEntity.ok("hello");
    }

    @GetMapping
    @ResponseBody
    public List<customer> Get()
    {                 
        List<customer> list = new ArrayList<>();
        list = vCustomerService.findAll();
        return list;
    }

    @PostMapping
    public ResponseEntity Create(@Valid @RequestBody final customer vCustomer)
    {
        return ResponseEntity.ok(vCustomerService.save(vCustomer));
    }

    @GetMapping("/{Id}")
    public ResponseEntity<customer> Get(@PathVariable final Long Id)
    {
        final Optional<customer> cek = vCustomerService.findById(Id);

        if(!cek.isPresent())
        {
            // log.error("Id " + Id + " tidak ada");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(cek.get());
    }

    @PutMapping("/{Id}")
    public ResponseEntity<customer> Update(@PathVariable final Long Id, @Valid @RequestBody final customer vCustomer)
    {
        if (!vCustomerService.findById(Id).isPresent()) {
            // log.error("Id " + ID + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(vCustomerService.save(vCustomer));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable final Long id) {
        if (!vCustomerService.findById(id).isPresent()) {
            // log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        vCustomerService.deleteById(id);

        return ResponseEntity.ok().build();
    }
}