package com.example.restapi.customer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service

@RequiredArgsConstructor
public class customerService{
    private final customerRepository vCustomerRespository; 

    public customerService(customerRepository _vCustomerRespository) {
        this.vCustomerRespository = _vCustomerRespository;
    }

    public List<customer> findAll()
    {
        // List<customer> list = new ArrayList<>();
        // Iterable<customer> findAll = vCustomerRespository.findAll();
        // findAll.forEach(list::add);
        // return list;

        return vCustomerRespository.findAll();
    }

    public Optional<customer> findById(Long Id)
    {
        return vCustomerRespository.findById(Id);
    }

    public customer save(customer newCust)
    {
        return vCustomerRespository.save(newCust);
    }

    public void deleteById(Long Id)
    {
        vCustomerRespository.deleteById(Id);
    }
}